; program to light on LEDs !

    add  r0,30,r1   ; 30 in R1
    add  r1,12,r2   ; 42 in R2
    add  r0,r2,r1   ; 42 in R1
    sub  r1,2,r1    ; 40 in R1
    add  r1,2,r1    ; 42 in R1
    mul  r1,2,r2    ; 84 in R2
    sub  r2,81,r3   ;  3 in R3
    mul  r3,14,r1   ; 42 in r1
    seq  r1,42,r3   ;  1 in r3
    branz r3, affiche
    add r0,15,r1
    jmp loop,r0

affiche:
    scall 1

loop:
    stop
    jmp loop,r0
