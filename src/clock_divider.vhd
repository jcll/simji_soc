library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_divider is
port (
  reset_n      : in  std_logic;
  clk100       : in  std_logic;
  clk50        : out  std_logic
 );
end entity;

architecture arch of clock_divider is
  signal clk50_s : std_logic;
begin

  reg : process(reset_n,clk100)
  begin
    if reset_n='0' then
      clk50_s <= '0';
    elsif rising_edge(clk100) then
      clk50_s <= not(clk50_s);
    end if;
  end process;

  clk50 <= clk50_s;
end arch;
