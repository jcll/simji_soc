--------------------------------------------------------------------------------
-- Generated automatically by Reggae compiler
-- (c) Jean-Christophe Le Lann - 2011
-- date : Wed Apr  3 15:21:59 2019
--------------------------------------------------------------------------------
library ieee,std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library uart_lib;
library misc_lib;

entity soc is
  port(
    reset_n : in std_logic;
    clk     : in  std_logic;
    rx      : in  std_logic;
    tx      : out std_logic;
    leds    : out std_logic_vector(15 downto 0)
    ;
    -- --
     bypass_uart    : in std_logic;
     bypass_ce      : in std_logic;
     bypass_we      : in std_logic;
     bypass_address : in unsigned(7 downto 0);
     bypass_datain  : in std_logic_vector(31 downto 0);
     bypass_dataout : out std_logic_vector(31 downto 0)
  );
end entity;

architecture rtl of soc is
  -- from UART
  signal uart_to_bus_ce      : std_logic;
  signal uart_to_bus_we      : std_logic;
  signal uart_to_bus_address : unsigned(7 downto 0);
  signal uart_to_bus_data    : std_logic_vector(31 downto 0);
  signal bus_to_uart_data    : std_logic_vector(31 downto 0);
  -- bus
  signal bus_ce      : std_logic;
  signal bus_we      : std_logic;
  signal bus_address : unsigned(7 downto 0);
  signal bus_datain  : std_logic_vector(31 downto 0);
  signal bus_dataout : std_logic_vector(31 downto 0);
  --
  signal sreset  : std_logic;
  -- debug
  signal slow_clk,slow_tick : std_logic;

  -- various signals for
  signal proc_to_instr_ram_sel : std_logic;
  signal proc_to_instr_ram_we  : std_logic;
  signal proc_to_instr_ram_address : unsigned(7 downto 0);
  signal instr_ram_to_proc_data : std_logic_vector(31 downto 0);

  signal proc_to_data_ram_sel : std_logic;
  signal proc_to_data_ram_we : std_logic;
  signal proc_to_data_ram_address : unsigned(7 downto 0);
  signal proc_to_data_ram_data : std_logic_vector(31 downto 0);
  signal data_ram_to_proc_data : std_logic_vector(31 downto 0);
  signal dataout_ram_data : std_logic_vector(31 downto 0);
  signal dataout_ram_instr : std_logic_vector(31 downto 0);
  signal dataout_processor : std_logic_vector(31 downto 0);
  --

  --
  signal leds_processor : std_logic_vector(15 downto 0);
  signal stopped : std_logic;
  signal running : std_logic;
begin

  --sreset <= '0';--need to be connected to control register. which one ?

  sreset <= not(reset_n);--just to test. influence on BRAM

  -- ============== UART as Master of bus !=========
  uart_master : entity uart_lib.uart_bus_master
    generic map (
      ADDR_WIDTH => 8,
      DATA_WIDTH => 32)
    port map(
      reset_n => reset_n,
      clk     => clk,
      -- UART --
      rx      => rx,
      tx      => tx,
      -- Bus --
      ce      => uart_to_bus_ce,
      we      => uart_to_bus_we,
      address => uart_to_bus_address,
      datain  => uart_to_bus_data,
      dataout => bus_to_uart_data
      );

  --simulation DEBUG
      bus_ce         <= bypass_ce;
      bus_we         <= bypass_we;
      bus_address    <= bypass_address;
      bus_datain     <= bypass_datain;
      bypass_dataout <= bus_to_uart_data;
  -- --
  --   bus_ce      <= uart_to_bus_ce;
  --   bus_we      <= uart_to_bus_we;
  --   bus_address <= uart_to_bus_address;
  --   bus_datain  <= uart_to_bus_data;


  -- OR-ing results from various IPs
  bus_to_uart_data <= dataout_ram_instr or dataout_ram_data or dataout_processor;
  -- ==================ram_instr===================
  inst_ram_instr : entity work.ram_instr
    port map (
      reset_n => reset_n,
      clk     => clk,
      sreset  => sreset,
      ce      => bus_ce,
      we      => bus_we,
      address => bus_address,
      datain  => bus_datain,
      dataout => dataout_ram_instr,
      ----------------------------------------------
      proc_to_mem_ce      => proc_to_instr_ram_sel,
      proc_to_mem_we      => proc_to_instr_ram_we,
      proc_to_mem_address => proc_to_instr_ram_address,
      mem_to_proc_data    => instr_ram_to_proc_data
    );

  -- ===================ram_data===================
  inst_ram_data : entity work.ram_data
    port map (
      reset_n => reset_n,
      clk     => clk,
      sreset  => sreset,
      ce      => bus_ce,
      we      => bus_we,
      address => bus_address,
      datain  => bus_datain,
      dataout => dataout_ram_data,
      ----------------------------------------------
      proc_to_mem_ce      => proc_to_data_ram_sel,
      proc_to_mem_we      => proc_to_data_ram_we,
      proc_to_mem_address => proc_to_data_ram_address,
      proc_to_mem_data    => proc_to_data_ram_data,
      mem_to_proc_data    => data_ram_to_proc_data
    );

  --==================processor=====================
  inst_processor : entity work.processor
    port map (
      reset_n => reset_n,
      clk     => clk,
      sreset  => sreset,
      -- bus
      ce      => bus_ce,
      we      => bus_we,
      address => bus_address,
      datain  => bus_datain,
      dataout => dataout_processor,
      --------------------------------------------
      code_sel     => proc_to_instr_ram_sel,
      code_we      => proc_to_instr_ram_we,
      code_address => proc_to_instr_ram_address,
      code_in      => instr_ram_to_proc_data,
      --------------------------------------------
      data_sel     => proc_to_data_ram_sel,
      data_we      => proc_to_data_ram_we,
      data_address => proc_to_data_ram_address,
      data_out     => proc_to_data_ram_data,
      data_in      => data_ram_to_proc_data,
      --
      leds         => leds_processor,
      stopped      => stopped,
      running      => running
    );

  -- =================== DEBUG ====================
  ticker : entity misc_lib.slow_ticker(rtl)
    port map(
      reset_n   => reset_n,
      fast_clk  => clk,
      slow_clk  => slow_clk,
      slow_tick => slow_tick
      );

  leds <= slow_clk & running & stopped & leds_processor(12 downto 0);--WARN : 2 bits missing

end;
