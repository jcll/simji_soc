require_relative "basic_interact"

if ARGV.size!=2
  puts "ERROR : two arguments needed : code_filename data_filename (in this order)"
  puts " example : ruby play.rb simji_progs/matrix_3x3.bin simji_progs/matrix_data.bin"
else
  code_filename,data_filename=ARGV[0..1]
  puts "code is '#{code_filename}'"
  puts "data is '#{data_filename}'"
  download_and_check(code_filename,data_filename)
  start_simji
  get_status
end
