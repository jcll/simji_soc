require_relative "basic_interact"

str_addr=ARGV.first
if str_addr.start_with?("0x")
  addr=str_addr[2..-1].to_i(16)
  puts "reading reg at address 0x#{addr.to_s(16)}"
  val=read_reg(addr)
  puts "got value : 0x"+val.to_s(16)+" (0b#{val.to_s(2).rjust(8,'0')})"
else
  puts "wrong format : address muts start with '0x' "
end
