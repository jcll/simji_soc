--------------------------------------------------------------------------------
-- Generated automatically by Reggae compiler
-- (c) Jean-Christophe Le Lann - 2011
-- date : Wed Apr  3 15:21:59 2019
--------------------------------------------------------------------------------
library ieee,std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.processor_pkg.all;

entity processor is
  port(
    reset_n : in  std_logic;
    clk     : in  std_logic;
    sreset  : in  std_logic;
    ce      : in  std_logic;
    we      : in  std_logic;
    address : in  unsigned(7 downto 0);
    datain  : in  std_logic_vector(31 downto 0);
    dataout : out std_logic_vector(31 downto 0));
end processor;

architecture RTL of processor is

  --interface
  signal regs      : registers_type;
  signal sampling  : sampling_type;

begin

  regif_inst : entity work.processor_reg
    port map(
      reset_n   => reset_n,
      clk       => clk,
      sreset    => sreset,
      ce        => ce,
      we        => we,
      address   => address,
      datain    => datain,
      dataout   => dataout,
      registers => regs,
      sampling  => sampling
    );

 core_i : entity simji_core.core
    port map(
    reset_n     =>  reset_n,--: in  std_logic;
    clk         =>  clk,--: in  std_logic;
    boot_address=>  regs.boot_address,--: in  unsigned(CODE_ADDR_WIDTH-1 downto 0);
    go          =>  regs.control.go,--: in  std_logic;
    stopped     =>  regs.status.stopped,--: out std_logic;
    -----------------------------------------------------------
    code_sel    =>  ,--: out std_logic;
    code_we     =>  ,--: out std_logic;
    code_address=>  ,--: out unsigned(CODE_ADDR_WIDTH-1 downto 0);
    code_in     =>  ,--: in  std_logic_vector(31 downto 0);
    ----------------------------------------------------------
    data_sel    =>  ,--: out std_logic;
    data_we     =>  ,--: out std_logic;
    data_address=>  ,--: out unsigned(DATA_ADDR_WIDTH-1 downto 0);
    data_out    =>  ,--: out std_logic_vector(31 downto 0);
    data_in     =>  ,--: in  std_logic_vector(31 downto 0));
    );

end RTL;
