echo "=> cleaning"
rm -rf *.o *.cf soc_tb
echo "---------------- compiling assets ----------------"
echo "=> ../src/assets/flag_buf.vhd"
ghdl -a --work=uart_lib ../src/assets/flag_buf.vhd
echo "=> ../src/assets/mod_m_counter.vhd"
ghdl -a --work=uart_lib ../src/assets/mod_m_counter.vhd
echo "=> ../src/assets/slow_ticker.vhd"
ghdl -a --work=misc_lib ../src/assets/slow_ticker.vhd
echo "=> ../src/assets/fifo.vhd"
ghdl -a --work=uart_lib ../src/assets/fifo.vhd
echo "=> ../src/assets/uart_rx.vhd"
ghdl -a --work=uart_lib ../src/assets/uart_rx.vhd
echo "=> ../src/assets/uart_tx.vhd"
ghdl -a --work=uart_lib ../src/assets/uart_tx.vhd
echo "=> ../src/assets/uart.vhd"
ghdl -a --work=uart_lib ../src/assets/uart.vhd
echo "=> ../src/assets/uart_bus_master.vhd"
ghdl -a --work=uart_lib ../src/assets/uart_bus_master.vhd

echo "=> ---------------- SIMJI Softcore ----------------"
SIMJI_CORE_DIR=/home/jcll/FORGE/JCLL/dev/HW_DESIGN/simji_softcore
echo "=> txt_util.vhd"
ghdl -a --work=misc_lib $SIMJI_CORE_DIR/src/txt_util.vhd
echo "=> tunnels.vhd"
ghdl -a --work=misc_lib $SIMJI_CORE_DIR/src/tunnels.vhd
echo "=> type_package.vhd"
ghdl -a --work=simji_core $SIMJI_CORE_DIR/src/type_package.vhd
echo "=> core_entity.vhd"
ghdl -a --work=simji_core $SIMJI_CORE_DIR/src/core_entity.vhd
echo "=> core_arch_pipelined_v0.vhd"
ghdl -a --work=simji_core $SIMJI_CORE_DIR/src/core_arch_pipelined_v0.vhd

echo "=> ---------------- SIMJI Soc on FPGA ----------------"
echo "=> bram_xilinx.vhd"
ghdl -a ../src/bram_xilinx.vhd
echo "=> clock_divider.vhd"
ghdl -a --work=misc_lib ../src/clock_divider.vhd
echo "=> processor_pkg.vhd"
ghdl -a ../src/processor_pkg.vhd
echo "=> processor_reg.vhd"
ghdl -a ../src/processor_reg.vhd
echo "=> processor.vhd"
ghdl -a ../src/processor.vhd
echo "=> ram_data_pkg.vhd"
ghdl -a ../src/ram_data_pkg.vhd
echo "=> ram_data_reg.vhd"
ghdl -a ../src/ram_data_reg.vhd
echo "=> ram_data.vhd"
ghdl -a ../src/ram_data.vhd
echo "=> ram_instr_pkg.vhd"
ghdl -a ../src/ram_instr_pkg.vhd
echo "=> ram_instr_pkg.vhd"
ghdl -a ../src/ram_instr_reg.vhd
echo "=> ram_instr.vhd"
ghdl -a ../src/ram_instr.vhd
echo "=> soc_bypass_uart.vhd"
ghdl -a ../src/soc_bypass_uart.vhd
echo "=> ---------------- testbench SIMJI----------------"
echo "=> soc_tb.vhd"
ghdl -a --ieee=synopsys ../tb/soc_tb.vhd
echo "=> elaboration..."
ghdl -e --ieee=synopsys soc_tb
echo "=> ---------------- RUN testbench SIMJI----------------"
if test -f "soc_tb"; then
    ghdl -r soc_tb --wave=soc_tb.ghw
    gtkwave soc_tb.ghw soc_tb.sav
else
    echo "soc_tb does not exist"
fi
