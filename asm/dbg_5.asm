add r0,4096,r2

decr:
  sub r2,1,r2   ; on decremente r2
  seq r2,42,r3  ; on test si r2== 42
  braz r3,decr  ; sinon on boucle

print:
  add r0,r2,r1  ; mov r2 => r1
  scall 1       ; doit afficher 42

loop:
  stop
  jmp loop,r0
