--------------------------------------------------------------------------------
-- Generated automatically by Reggae compiler
-- (c) Jean-Christophe Le Lann - 2011
-- date : Wed Apr  3 15:21:59 2019
--------------------------------------------------------------------------------
library ieee,std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ram_data_pkg.all;

entity ram_data is
  port(
    reset_n : in  std_logic;
    clk     : in  std_logic;
    sreset  : in  std_logic;
    -- bus
    ce      : in  std_logic;
    we      : in  std_logic;
    address : in  unsigned(7 downto 0);
    datain  : in  std_logic_vector(31 downto 0);
    dataout : out std_logic_vector(31 downto 0);
    -- processor
    proc_to_mem_ce      : in  std_logic;
    proc_to_mem_we      : in  std_logic;
    proc_to_mem_address : in  unsigned(7 downto 0);
    proc_to_mem_data    : in  std_logic_vector(31 downto 0);
    mem_to_proc_data    : out std_logic_vector(31 downto 0)
    );
end ram_data;

architecture RTL of ram_data is

  --interface
  signal regs      : registers_type;
  signal sampling  : sampling_type;
  --
  signal ram_en : std_logic;
  signal ram_wr : std_logic;
  signal ram_address : std_logic_vector(9 downto 0);
  signal ram_datain  : std_logic_vector(31 downto 0);
  signal ram_dataout : std_logic_vector(31 downto 0);

begin

  regif_inst : entity work.ram_data_reg
    port map(
      reset_n   => reset_n,
      clk       => clk,
      sreset    => sreset,
      ce        => ce,
      we        => we,
      address   => address,
      datain    => datain,
      dataout   => dataout,
      registers => regs,
      sampling  => sampling
    );

  --=========================================
  ram_en      <= regs.control.en when regs.control.mode='0' else proc_to_mem_ce;
  ram_wr      <= (regs.control.en and regs.control.wr) when regs.control.mode='0' else proc_to_mem_we;
  ram_address <= regs.address.value when regs.control.mode='0' else std_logic_vector("00" & proc_to_mem_address);
  ram_datain  <= regs.datain.value  when regs.control.mode='0' else proc_to_mem_data;

  bram : entity work.bram_xilinx
    generic map(
      nbits_addr => 10,
      nbits_data => 32
    )
    port map(
      clk     => clk,
      sreset  => sreset,
      en      => ram_en,
      we      => ram_wr,
      address => ram_address,
      datain  => ram_datain,
      dataout => ram_dataout
    );

   mem_to_proc_data <= ram_dataout;

   sampling.dataout_value <= ram_dataout;

end RTL;
