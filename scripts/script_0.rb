require 'pp'
require "rubyserial"
require 'colorize'

require_relative "uart"

def create_random_array size=256,data_w=32
  puts "=> creating random array size=#{size} data bitwidth=#{data_w}"
  Array.new(size){|e| rand(0..2**data_w-1)}
end

def write_code_mem data
  puts "=> writing to code mem..."
  data.each_with_index do |data,addr|
    write_reg(0x0,addr)
    write_reg(0x1,data)
    write_reg(0x3,0b11) #UART Bus master
    #puts "#{hexa(addr)} #{hexa(data)}"
  end
end

def read_code_mem nb_data=256
  puts "=> reading code mem..."
  result=[]
  nb_data=256 if nb_data>256
  for addr in 0..nb_data-1
    write_reg(0x0,addr)
    write_reg(0x3,0x01) # read
    result << read_reg(0x2)
  end
  result
end

def write_data_mem data
  puts "=> writing to data mem..."
  data.each_with_index do |data,addr|
    write_reg(0x4,addr)
    write_reg(0x5,data)
    write_reg(0x7,0b11) #UART Bus master
    #puts "#{hexa(addr)} #{hexa(data)}"
  end
end

def read_data_mem nb_data=256
  puts "=> reading data mem..."
  result=[]
  nb_data=256 if nb_data>256
  for addr in 0..nb_data-1
    write_reg(0x4,addr)
    write_reg(0x7,0x01) # read
    result << read_reg(0x6)
  end
  result
end

def check_equal ram1,ram2
  puts "=> checking... "
  errors=0
  if ram1.size!=ram2.size
    puts "\t-ram sizes differs."
    return false
  else
    for addr in 0..ram1.size-1
      if (v1=ram1[addr])!=(v2=ram2[addr])
        errors+=1
        puts "\t- error @#{addr} : #{hexa(v1)} vs #{hexa(v2)}"
      end
    end
  end
  if errors>0
    puts "\t- rams differs."
    return false
  end
  puts "\t- rams are identical"
  return true
end

def check_data_ram nb_data=10
  data=create_random_array(nb_data)
  write_data_mem(data)
  result=read_data_mem(nb_data)
  check_equal data,result
end

def check_code_ram nb_data=10
  code=create_random_array(nb_data)
  write_code_mem(code)
  result=read_code_mem(nb_data)
  check_equal code,result
end

puts "writing 123 @0x0"
write_reg(0x0,123)
puts "reading ?"
p read_reg(0x0)

puts "writing 123 @0x4"
write_reg(0x4,123)
puts "reading ?"
p read_reg(0x4)


check_code_ram
check_code_ram
check_data_ram
check_data_ram
