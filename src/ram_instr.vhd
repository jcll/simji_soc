--------------------------------------------------------------------------------
-- Generated automatically by Reggae compiler
-- (c) Jean-Christophe Le Lann - 2011
-- date : Mon Apr  8 17:04:17 2019
--------------------------------------------------------------------------------
library ieee, std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ram_instr_pkg.all;

entity ram_instr is
  generic (
    nbits_addr : natural := 8;
    nbits_data : natural := 32
    );
  port(
    reset_n : in  std_logic;
    clk     : in  std_logic;
    sreset  : in  std_logic;
    ce      : in  std_logic;
    we      : in  std_logic;
    address : in  unsigned(7 downto 0);
    datain  : in  std_logic_vector(31 downto 0);
    dataout : out std_logic_vector(31 downto 0);
    -- processor
    proc_to_mem_ce      : in  std_logic;
    proc_to_mem_we      : in  std_logic;
    proc_to_mem_address : in  unsigned(7 downto 0);
    mem_to_proc_data    : out std_logic_vector(31 downto 0)
    );
end ram_instr;

architecture RTL of ram_instr is

  --interface
  signal regs     : registers_type;
  signal sampling : sampling_type;

  signal bram_we      : std_logic;
  signal bram_en      : std_logic;
  signal bram_address : std_logic_vector(nbits_addr-1 downto 0);
  signal bram_datain  : std_logic_vector(nbits_data-1 downto 0);
  signal bram_dataout : std_logic_vector(nbits_data-1 downto 0);
begin

  regif_inst : entity work.ram_instr_reg
    port map(
      reset_n   => reset_n,
      clk       => clk,
      sreset    => sreset,
      ce        => ce,
      we        => we,
      address   => address,
      datain    => datain,
      dataout   => dataout,
      registers => regs,
      sampling  => sampling
      );

  bram_xilinx_1 : entity work.bram_xilinx
    generic map (
      nbits_addr => nbits_addr,
      nbits_data => nbits_data)
    port map (
      clk     => clk,
      we      => bram_we,
      en      => bram_en,
      address => bram_address,
      datain  => bram_datain,
      dataout => bram_dataout);

  bram_en      <= regs.control.en    when regs.control.mode='0' else proc_to_mem_ce;
  bram_we      <= regs.control.wr    when regs.control.mode='0' else proc_to_mem_we;
  bram_address <= regs.address.value when regs.control.mode='0' else std_logic_vector(proc_to_mem_address);
  bram_datain  <= regs.datain.value  when regs.control.mode='0' else (others=>'0');
  mem_to_proc_data <= bram_dataout;
  sampling.dataout_value <= bram_dataout;

end RTL;
