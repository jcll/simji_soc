--------------------------------------------------------------------------------
-- Generated automatically by Reggae compiler
-- (c) Jean-Christophe Le Lann - 2011
-- date : Wed Apr  3 15:21:59 2019
--------------------------------------------------------------------------------
library ieee,std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package processor_pkg is

  type boot_address_reg is record
    value : std_logic_vector(9 downto 0);
  end record;

  constant BOOT_ADDRESS_INIT: boot_address_reg :=(
    value => "0000000000");

  type control_reg is record
    init : std_logic;
    go   : std_logic;
  end record;

  constant CONTROL_INIT: control_reg :=(
    init => '0',
    go   => '0');

  type status_reg is record
    stopped : std_logic;
  end record;

  constant STATUS_INIT: status_reg :=(
    stopped => '0');

  type registers_type is record
    boot_address : boot_address_reg; -- 0x8
    control      : control_reg; -- 0x9
    status       : status_reg; -- 0xa
  end record;

  constant REGS_INIT : registers_type :=(
    boot_address => BOOT_ADDRESS_INIT,
    control      => CONTROL_INIT,
    status       => STATUS_INIT);

  --sampling values from IPs
  type sampling_type is record
    stopped : std_logic;
  end record;

end package;
