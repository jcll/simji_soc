require 'pp'
require "rubyserial"
require 'colorize'

$serial = Serial.new '/dev/ttyUSB1', 19200,8

def read_reg_ary address
  $serial.write [           0x10].pack("C*")
  $serial.write [           address].pack("C*")
  reg=[]
  for i in 0..3
    byte=nil
    byte=$serial.getbyte until byte
    reg << byte
  end
  reg
end

def read_reg address
  val_ary=read_reg_ary(address)
  byte_array_to_int(val_ary)
end

def write_reg_ary addr,data=[]
  $serial.write [            0x11].pack("C*")
  $serial.write [            addr].pack("C*")
  $serial.write data.pack("CCCC")
end

def write_reg addr,data
  write_reg_ary addr, int_to_byte_array(data,4)
end

def hit_a_key
  puts "> hit a key <"
  key=$stdin.gets.chomp
end

def int_to_byte_array num, bytes
  (bytes - 1).downto(0).collect{|byte| (num >> (byte * 8)) & 0xFF}
end

def byte_array_to_int ary
  res=0
  ary.reverse.each_with_index do |byte,idx|
    res+=byte << idx*8
  end
  res
end

def hexa x
  "0x"+x.to_s(16)
end

# iram access through registers :
# reg 0x0 is address
# reg 0x1 is datain
# reg 0x2 is dataout
# reg 0x3 is control
#  control 0 is enable
#  control 1 is wr
#  control 2 is reset (UNCONNECTED)
#  control 3 is mode (0 is access from host)
def write_instr_ram addr=[],data=[]
  write_reg_ary 0x0,addr
  write_reg_ary 0x1,data
  write_reg_ary 0x3,[0,0,0,0b0011]
end

def read_instr_ram addr=[]
  write_reg_ary 0x0,addr
  write_reg_ary 0x3,[0,0,0,0b0001]
  read_reg_ary 0x2
end

# ram data access through registers :
# reg 0x4 is address
# reg 0x5 is datain
# reg 0x6 is dataout
# reg 0x7 is control
#  control 0 is enable
#  control 1 is wr
#  control 2 is reset (UNCONNECTED)
#  control 3 is mode (0 is access from host)
def write_data_ram addr=[],data=[]
  write_reg_ary 0x4,addr # BUG fix : was 0x0
  write_reg_ary 0x5,data
  write_reg_ary 0x7,[0,0,0,0b0011]
end

def read_data_ram addr
  write_reg_ary 0x4,addr #BUG fix : was 0x0
  write_reg_ary 0x7,[0,0,0,0b0001]
  read_reg_ary 0x6
end

def show ram
  ram.each do |addr,bytes|
    addr=addr.to_s.rjust(5)
    data=bytes.collect{|byte| "0x"+byte.to_s(16).rjust(2,'0')}.join(" ")
    puts "#{addr} : #{data}"
  end
end

def format ary
  ary.collect{|byte| byte.to_s(16).rjust(2,'0')}.join
end
