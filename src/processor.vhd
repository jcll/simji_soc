--------------------------------------------------------------------------------
-- Generated automatically by Reggae compiler
-- (c) Jean-Christophe Le Lann - 2011
-- date : Mon Apr  8 17:04:17 2019
--------------------------------------------------------------------------------
library ieee,std;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.processor_pkg.all;

library simji_core;

entity processor is
  generic(
    CODE_ADDR_WIDTH : natural :=8;
    DATA_ADDR_WIDTH : natural :=8
  );
  port(
    reset_n : in  std_logic;
    clk     : in  std_logic;
    sreset  : in  std_logic;
    ce      : in  std_logic;
    we      : in  std_logic;
    address : in  unsigned(7 downto 0);
    datain  : in  std_logic_vector(31 downto 0);
    dataout : out std_logic_vector(31 downto 0);

    ---------------------------------------------------------------------------
    code_sel      : out std_logic;
    code_we       : out std_logic;
    code_address  : out unsigned(CODE_ADDR_WIDTH-1 downto 0);
    code_in       : in  std_logic_vector(31 downto 0);
    ---------------------------------------------------------------------------
    data_sel      : out std_logic;
    data_we       : out std_logic;
    data_address  : out unsigned(DATA_ADDR_WIDTH-1 downto 0);
    data_out      : out std_logic_vector(31 downto 0);
    data_in       : in  std_logic_vector(31 downto 0);
    --
    leds          : out std_logic_vector(15 downto 0);
    stopped       : out std_logic;
    running       : out std_logic
    );
end processor;

architecture RTL of processor is

  --interface
  signal regs      : registers_type;
  signal sampling  : sampling_type;

  signal stopped_s : std_logic;
  signal leds_s : std_logic_vector(15 downto 0);
  signal has_received_go : std_logic;

  signal r1_7bits : std_logic_vector(6 downto 0);
begin

  regif_inst : entity work.processor_reg
    port map(
      reset_n   => reset_n,
      clk       => clk,
      sreset    => sreset,
      ce        => ce,
      we        => we,
      address   => address,
      datain    => datain,
      dataout   => dataout,
      registers => regs,
      sampling  => sampling
    );

   core_i : entity simji_core.core(archi_v0)
     port map(
       reset_n     =>  reset_n,
       clk         =>  clk,
       boot_address=>  unsigned(regs.boot_address.value(7 downto 0)),--: in  unsigned(CODE_ADDR_WIDTH-1 downto 0);
       go          =>  regs.control.go,
       stopped     =>  stopped_s,
       running     =>  running,
       -----------------------------------------------------------
       code_sel    =>  code_sel    ,
       code_we     =>  code_we     ,
       code_address=>  code_address,
       code_in     =>  code_in     ,
       -----------------------------
       data_sel    =>  data_sel    ,
       data_we     =>  data_we     ,
       data_address=>  data_address,
       data_out    =>  data_out    ,
       data_in     =>  data_in     ,
       leds        =>  leds_s
     );

     spy_p:process(reset_n,clk)
     begin
       if reset_n='0' then
         has_received_go <= '0';
       elsif rising_edge(clk) then
         if regs.control.go='1' then
           has_received_go <= '1';
         end if;
       end if;
     end process;

     leds <= leds_s(15 downto 0);
     --r1_7bits <= leds_s(6 downto 0);
     r1_7bits <= (others=>'0'); --leds_s(6 downto 0);

     sampling.stopped <= stopped_s;
     sampling.r1_7bits <= r1_7bits;
end RTL;
