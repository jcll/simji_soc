start:
      add r0,1,r1       # 1 in R1
      scall 1

init_r4:
      add r0,8192,r4
init_r2:
      add r0,8182,r2
wait_r2:
      sub r2,1,r2
      branz r2,wait_r2
dec_r4:
      sub r4,1,r4
      branz r4,init_r2

affiche:
      mul r1,2,r1
      seq r1,2048,r3
      branz r3,start
      scall 1
      jmp start,r0
loop:
      stop
      jmp loop,r0
