echo "=== testbench ==="
echo "=> cleaning..."
rm -rf *.o *.cf soc_tb soc_tb.ghw

echo "=> compiling VHDL files MISC_LIB IPs and utilities..."
ghdl -a --work=misc_lib ../soc/assets/slow_ticker.vhd
ghdl -a --work=misc_lib ./txt_util.vhd
ghdl -a --work=misc_lib ../../simji_softcore/src/tunnels.vhd

echo "=> compiling VHDL files UART_LIB ..."
ghdl -a --work=uart_lib ../soc/assets/fifo.vhd
ghdl -a --work=uart_lib ../soc/assets/flag_buf.vhd
ghdl -a --work=uart_lib ../soc/assets/mod_m_counter.vhd
ghdl -a --work=uart_lib ../soc/assets/uart_rx.vhd
ghdl -a --work=uart_lib ../soc/assets/uart_tx.vhd
ghdl -a --work=uart_lib ../soc/assets/uart.vhd
ghdl -a --work=uart_lib ../soc/assets/uart_bus_master.vhd

echo "=> compiling VHDL files SIMJI_CORE ..."
ghdl -a --work=simji_core ../../simji_softcore/src/type_package.vhd
ghdl -a --work=simji_core ../../simji_softcore/src/core_entity.vhd
ghdl -a --work=simji_core ../../simji_softcore/src/core_arch_pipelined_v0.vhd

echo "=> compiling VHDL files SIMJI_SOC ..."
ghdl -a --work=simji_soc ../soc/bram_xilinx.vhd
ghdl -a --work=simji_soc ../soc/ram_data_pkg.vhd
ghdl -a --work=simji_soc ../soc/ram_data_reg.vhd
ghdl -a --work=simji_soc ../soc/ram_data.vhd
ghdl -a --work=simji_soc ../soc/ram_instr_pkg.vhd
ghdl -a --work=simji_soc ../soc/ram_instr_reg.vhd
ghdl -a --work=simji_soc ../soc/ram_instr.vhd
ghdl -a --work=simji_soc ../soc/processor_pkg.vhd
ghdl -a --work=simji_soc ../soc/processor_reg.vhd
ghdl -a --work=simji_soc ../soc/processor_v0.vhd
ghdl -a --work=simji_soc ../soc/soc.vhd

echo "=> compiling VHDL files Testbench SIMJI_SOC ..."
ghdl -a --work=simji_soc --ieee=synopsys soc_tb.vhd

echo "=> elaboration of testbench SIMJI_SOC ..."
ghdl -e --work=simji_soc --ieee=synopsys soc_tb

echo "=> running testbench SIMJI_SOC ..."
ghdl -r soc_tb --wave=soc_tb.ghw

echo "=> viewing waveforms..."
gtkwave soc_tb.ghw soc_tb.sav
