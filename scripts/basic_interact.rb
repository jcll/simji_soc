require 'pp'
require "rubyserial"
require 'colorize'

$serial = Serial.new '/dev/ttyUSB1', 19200,8

def read_reg_ary address
  $serial.write [           0x10].pack("C*")
  $serial.write [           address].pack("C*")
  reg=[]
  for i in 0..3
    byte=nil
    byte=$serial.getbyte until byte
    reg << byte
  end
  reg
end

def read_reg address
  val_ary=read_reg_ary(address)
  byte_array_to_int(val_ary)
end

def write_reg_ary addr,data=[]
  $serial.write [            0x11].pack("C*")
  $serial.write [            addr].pack("C*")
  $serial.write data.pack("CCCC")
end

def write_reg addr,data
  write_reg_ary addr, int_to_byte_array(data,4)
end

def hit_a_key
  puts "> hit a key <"
  key=$stdin.gets.chomp
end

def int_to_byte_array num, bytes
  (bytes - 1).downto(0).collect{|byte| (num >> (byte * 8)) & 0xFF}
end

def byte_array_to_int ary
  res=0
  ary.reverse.each_with_index do |byte,idx|
    res+=byte << idx*8
  end
  res
end

# iram access through registers :
# reg 0x0 is address
# reg 0x1 is datain
# reg 0x2 is dataout
# reg 0x3 is control
#  control 0 is enable
#  control 1 is wr
#  control 2 is reset (UNCONNECTED)
#  control 3 is mode (0 is access from host)
def write_instr_ram addr=[],data=[]
  write_reg_ary 0x0,addr
  write_reg_ary 0x1,data
  write_reg_ary 0x3,[0,0,0,0b0011]
end

def read_instr_ram addr=[]
  write_reg_ary 0x0,addr
  write_reg_ary 0x3,[0,0,0,0b0001]
  read_reg_ary 0x2
end

# ram data access through registers :
# reg 0x4 is address
# reg 0x5 is datain
# reg 0x6 is dataout
# reg 0x7 is control
#  control 0 is enable
#  control 1 is wr
#  control 2 is reset (UNCONNECTED)
#  control 3 is mode (0 is access from host)
def write_data_ram addr=[],data=[]
  write_reg_ary 0x4,addr # BUG fix : was 0x0
  write_reg_ary 0x5,data
  write_reg_ary 0x7,[0,0,0,0b0011]
end

def read_data_ram addr
  write_reg_ary 0x4,addr #BUG fix
  write_reg_ary 0x7,[0,0,0,0b0001]
  read_reg_ary 0x6
end

def show ram
  ram.each do |addr,bytes|
    addr=addr.to_s.rjust(5)
    data=bytes.collect{|byte| "0x"+byte.to_s(16).rjust(2,'0')}.join(" ")
    puts "#{addr} : #{data}"
  end
end

def format ary
  ary.collect{|byte| byte.to_s(16).rjust(2,'0')}.join
end

require 'timeout'

DOTS=50

def test_ram test_id,kind,size
  log_hash={}
  log_hash[:ram]=kind
  timeout_in_seconds = 20
  current=nil
  dots=0
  errors=0
  error_event=nil
  ram=[]
  begin
    Timeout::timeout(timeout_in_seconds) do
      print "=> testing #{kind} "
      log=File.new("test_#{test_id}_#{kind}.log",'w')
      for addr in 0..size
        current=addr
        ram[addr]=data=[rand(256),rand(256),rand(256),rand(256)]
        case kind
        when :iram
          write_instr_ram addr,data
        when :dram
          write_data_ram addr,data
        end
        dataf=format(data)
        #hit_a_key
        #sleep 1
        case kind
        when :iram
          data_rd=read_instr_ram(addr)
        when :dram
          data_rd=read_data_ram(addr)
        end
        data_rdf=format(data_rd)
        if dataf!=data_rdf
          errors+=1
          error_event=true
          log.puts "#{addr.to_s.rjust(5)} #{dataf} #{data_rdf} ERROR"
          #puts "#{addr.to_s.rjust(5)} #{dataf.green} <-?-> #{data_rdf.red}"
        else
          log.puts "#{addr.to_s.rjust(5)} #{dataf} #{data_rdf}"
          #puts "#{addr.to_s.rjust(5)} #{dataf.green} <-?-> #{data_rdf.green}"
        end
        if (addr % (size/DOTS)==0)
          dots+=1
          if error_event
            print "|".red
            error_event=false
          else
            print "|".green
          end
        end
      end
      percent=100*(1-errors/ram.size.to_f)
      log_hash[:success]=percent.round(1)
      print "done ".rjust(9).green
      print "|"
      print "#{current} |".rjust(9)
      puts "#{percent.round(1)} %".rjust(9)
      return log_hash
    end
  rescue Timeout::Error
    percent=100*(1-errors/ram.size.to_f)
    print " "*(2+DOTS-dots)
    print "timeout ".rjust(7).red
    print "|"
    print "#{current} |".rjust(9)
  end
end

def hexa x
  "0x"+x.to_s(16).rjust(8,'0')
end

def download_and_check prog_file, data_file
  puts "=> transfering CODE to SoC Instruction RAM"
  data_wr=[]
  IO.readlines(prog_file).each do |line|
    addr,data=line.split.collect{|hexa| hexa[2..-1].to_i(16)}
    addr=int_to_byte_array(addr,4)
    data=int_to_byte_array(data,4)
    data_wr << byte_array_to_int(data)
    write_instr_ram(addr,data)
  end

  puts "=> reading back CODE ram..."
  data_rd=[]
  nb_errors=0
  for addr in 0..data_wr.size-1
    data_rd << byte_array_to_int(read_instr_ram([0,0,0,addr]))
    if (rd=data_rd[addr])!=(wr=data_wr[addr])
        nb_errors+=1
        puts "#{hexa(wr)} #{hexa(rd)} ERROR"
    else
        #puts "#{hexa(wr)} #{hexa(rd)}"
    end
  end
  if nb_errors==0
    puts "=> NO error detected ! Good !"
  else
    puts "=> #{nb_errors} found while reading back CODE ram. Leaving..."
    exit
  end

  puts "=> transfering DATA to SoC Data RAM"
  data_wr=[]
  IO.readlines(data_file).each do |line|
    addr,data=line.split.collect{|hexa| hexa[2..-1].to_i(16)}
    addr=int_to_byte_array(addr,4)
    data=int_to_byte_array(data,4)
    data_wr << byte_array_to_int(data)
    write_data_ram(addr,data)
  end

  puts "=> reading back data ram..."
  data_rd=[]
  nb_errors=0
  for addr in 0..data_wr.size-1
    data_rd << byte_array_to_int(read_data_ram([0,0,0,addr]))
    if (rd=data_rd[addr])!=(wr=data_wr[addr])
        puts "#{hexa(wr)} #{hexa(rd)} ERROR"
        nb_errors+=1
    else
        #puts "#{hexa(wr)} #{hexa(rd)}"
    end
  end

  if nb_errors==0
    puts "=> NO error detected ! Good !"
  else
    puts "exiting"
    exit
  end
end

def start_simji
  puts "=> giving SIMJI control over BRAMs (CODE & DATA)"
  write_reg 0x3,0x8 #CODE
  write_reg 0x7,0x8 #DATA
  puts "=> starting SIMJI processor (go!)"
  write_reg 0x9, 0x00000002
end

def get_status
  puts "=> reading processor status"
  status=read_reg(0xa)
  puts ret="0b"+status.to_s(2).rjust(8,'0')
  return status
end
